import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.concurrent.TimeUnit;
@Listeners (ITestListener.class)

public class Selenium_Java_TestNG {

    public static WebDriver driver;
    static String BaseURL = "https://dsp.eskimi.com/";
    static JavascriptExecutor js;
    @BeforeTest
    public static void Websetup() throws InterruptedException {
        System.setProperty("webdriver.chrome.driver",  "C:\\Users\\Fahim\\Desktop\\eskimi\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(BaseURL);
        Thread.sleep(3000);
        Assert.assertTrue(driver.getTitle().contains("Eskimi DSP - Your programmatic partner"));
        js = (JavascriptExecutor)driver;
    }
    @Test(priority = 0)
    public  static void User_Login() throws Exception {
        MyScreenRecorder.startRecording("");
        //username
        driver.findElement(By.xpath("//input[@id='username']")).click();
        driver.findElement(By.xpath("//input[@id='username']")).sendKeys("sqa-demo");
        Thread.sleep(1000);
        //Password
        driver.findElement(By.xpath("//input[@id='password']")).click();
        driver.findElement(By.xpath("//input[@id='password']")).sendKeys("demo123");
        Thread.sleep(1000);
        //Submit
        Robot r = new Robot();
        int keyCode =  KeyEvent.VK_ENTER;
        r.keyPress(keyCode);
        r.keyRelease(keyCode);
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        //Assert
        Assert.assertTrue(driver.getPageSource().contains("Eskimi DSP - Your programmatic partne"));
        System.out.println("Login Success");
        MyScreenRecorder.startRecording("");


    }

    @Test(priority = 1)
    public static void New_campaign_group_1() throws InterruptedException, AWTException {
    driver.findElement(new By.ByLinkText("New campaign group")).click();
    Thread.sleep(3000);
    driver.findElement(By.id("select2-js-campaign-type-container")).click();
    Thread.sleep(1000);
        Robot r = new Robot();
        int keyCode =  KeyEvent.VK_DOWN;
        r.keyPress(keyCode);
        r.keyRelease(keyCode);
        keyCode =  KeyEvent.VK_ENTER;
        r.keyPress(keyCode);
        r.keyRelease(keyCode);
    Thread.sleep(1000);
    ((JavascriptExecutor) driver).executeScript("window.scrollBy(0,250)", "");
    driver.findElement(By.xpath("//input[@id='name']")).sendKeys("Test-1");
    Thread.sleep(1000);
    driver.findElement(By.id("date-range-field")).click();
    Thread.sleep(1000);
    driver.findElement(By.id("date-range-field")).sendKeys("04 Jan, 2021 - 04 Jan, 2021");
    Thread.sleep(1000);
    driver.findElement(By.id("bid_currency")).sendKeys("0.4");
    ((JavascriptExecutor) driver).executeScript("window.scrollBy(0,450)", "");
    driver.findElement(By.id("budget_daily_currency")).sendKeys("100");
    driver.findElement(By.id("budget_total_currency")).sendKeys("500");
    ((JavascriptExecutor) driver).executeScript("window.scrollBy(0,1650)", "");
    Thread.sleep(1000);
    driver.findElement(By.xpath("//button[contains(text(),'Save')]")).click();


    }

    @Test(priority = 2)
    public static void New_campaign_group_2() throws InterruptedException, AWTException {
        driver.findElement(new By.ByLinkText("New campaign group")).click();
        Thread.sleep(3000);
        driver.findElement(By.id("select2-js-campaign-type-container")).click();
        Thread.sleep(1000);
        Robot r = new Robot();
        int keyCode =  KeyEvent.VK_DOWN;
        r.keyPress(keyCode);
        r.keyRelease(keyCode);
        keyCode =  KeyEvent.VK_ENTER;
        r.keyPress(keyCode);
        r.keyRelease(keyCode);
        Thread.sleep(1000);
        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0,250)", "");
        driver.findElement(By.xpath("//input[@id='name']")).sendKeys("Test-1");
        Thread.sleep(1000);
        driver.findElement(By.id("date-range-field")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("date-range-field")).sendKeys("04 Jan, 2021 - 04 Jan, 2021");
        Thread.sleep(1000);
        driver.findElement(By.id("bid_currency")).sendKeys("0.4");
        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0,450)", "");
        driver.findElement(By.id("budget_daily_currency")).sendKeys("100");
        driver.findElement(By.id("budget_total_currency")).sendKeys("500");
        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0,1650)", "");
        Thread.sleep(1000);
        driver.findElement(By.xpath("//button[contains(text(),'Save')]")).click();


    }

    @Test(priority = 3)
    public static void New_campaign_group_3() throws InterruptedException, AWTException {
        driver.findElement(new By.ByLinkText("New campaign group")).click();
        Thread.sleep(3000);
        driver.findElement(By.id("select2-js-campaign-type-container")).click();
        Thread.sleep(1000);
        Robot r = new Robot();
        int keyCode =  KeyEvent.VK_DOWN;
        r.keyPress(keyCode);
        r.keyRelease(keyCode);
        keyCode =  KeyEvent.VK_ENTER;
        r.keyPress(keyCode);
        r.keyRelease(keyCode);
        Thread.sleep(1000);
        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0,250)", "");
        driver.findElement(By.xpath("//input[@id='name']")).sendKeys("Test-1");
        Thread.sleep(1000);
        driver.findElement(By.id("date-range-field")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("date-range-field")).sendKeys("04 Jan, 2021 - 04 Jan, 2021");
        Thread.sleep(1000);
        driver.findElement(By.id("bid_currency")).sendKeys("0.4");
        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0,450)", "");
        driver.findElement(By.id("budget_daily_currency")).sendKeys("100");
        driver.findElement(By.id("budget_total_currency")).sendKeys("500");
        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0,1650)", "");
        Thread.sleep(1000);
        driver.findElement(By.xpath("//button[contains(text(),'Save')]")).click();


    }

    @Test(priority = 4)
    public static void New_campaign_group_4() throws InterruptedException, AWTException {
        driver.findElement(new By.ByLinkText("New campaign group")).click();
        Thread.sleep(3000);
        driver.findElement(By.id("select2-js-campaign-type-container")).click();
        Thread.sleep(1000);
        Robot r = new Robot();
        int keyCode =  KeyEvent.VK_DOWN;
        r.keyPress(keyCode);
        r.keyRelease(keyCode);
        keyCode =  KeyEvent.VK_ENTER;
        r.keyPress(keyCode);
        r.keyRelease(keyCode);
        Thread.sleep(1000);
        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0,250)", "");
        driver.findElement(By.xpath("//input[@id='name']")).sendKeys("Test-1");
        Thread.sleep(1000);
        driver.findElement(By.id("date-range-field")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("date-range-field")).sendKeys("04 Jan, 2021 - 04 Jan, 2021");
        Thread.sleep(1000);
        driver.findElement(By.id("bid_currency")).sendKeys("0.4");
        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0,450)", "");
        driver.findElement(By.id("budget_daily_currency")).sendKeys("100");
        driver.findElement(By.id("budget_total_currency")).sendKeys("500");
        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0,1650)", "");
        Thread.sleep(1000);
        driver.findElement(By.xpath("//button[contains(text(),'Save')]")).click();


    }

    @Test(priority = 5)
    public static void New_campaign_group_5() throws InterruptedException, AWTException {
        driver.findElement(new By.ByLinkText("New campaign group")).click();
        Thread.sleep(3000);
        driver.findElement(By.id("select2-js-campaign-type-container")).click();
        Thread.sleep(1000);
        Robot r = new Robot();
        int keyCode =  KeyEvent.VK_DOWN;
        r.keyPress(keyCode);
        r.keyRelease(keyCode);
        keyCode =  KeyEvent.VK_ENTER;
        r.keyPress(keyCode);
        r.keyRelease(keyCode);
        Thread.sleep(1000);
        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0,250)", "");
        driver.findElement(By.xpath("//input[@id='name']")).sendKeys("Test-1");
        Thread.sleep(1000);
        driver.findElement(By.id("date-range-field")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("date-range-field")).sendKeys("04 Jan, 2021 - 04 Jan, 2021");
        Thread.sleep(1000);
        driver.findElement(By.id("bid_currency")).sendKeys("0.4");
        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0,450)", "");
        driver.findElement(By.id("budget_daily_currency")).sendKeys("100");
        driver.findElement(By.id("budget_total_currency")).sendKeys("500");
        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0,1650)", "");
        Thread.sleep(1000);
        driver.findElement(By.xpath("//button[contains(text(),'Save')]")).click();


    }

    @Test(priority = 6)
    public static void New_campaign_group_6() throws InterruptedException, AWTException {
        driver.findElement(new By.ByLinkText("New campaign group")).click();
        Thread.sleep(3000);
        driver.findElement(By.id("select2-js-campaign-type-container")).click();
        Thread.sleep(1000);
        Robot r = new Robot();
        int keyCode =  KeyEvent.VK_DOWN;
        r.keyPress(keyCode);
        r.keyRelease(keyCode);
        keyCode =  KeyEvent.VK_ENTER;
        r.keyPress(keyCode);
        r.keyRelease(keyCode);
        Thread.sleep(1000);
        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0,250)", "");
        driver.findElement(By.xpath("//input[@id='name']")).sendKeys("Test-1");
        Thread.sleep(1000);
        driver.findElement(By.id("date-range-field")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("date-range-field")).sendKeys("04 Jan, 2021 - 04 Jan, 2021");
        Thread.sleep(1000);
        driver.findElement(By.id("bid_currency")).sendKeys("0.4");
        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0,450)", "");
        driver.findElement(By.id("budget_daily_currency")).sendKeys("100");
        driver.findElement(By.id("budget_total_currency")).sendKeys("500");
        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0,1650)", "");
        Thread.sleep(1000);
        driver.findElement(By.xpath("//button[contains(text(),'Save')]")).click();


    }

    @Test(priority = 7)
    public static void New_campaign_group_7() throws InterruptedException, AWTException {
        driver.findElement(new By.ByLinkText("New campaign group")).click();
        Thread.sleep(3000);
        driver.findElement(By.id("select2-js-campaign-type-container")).click();
        Thread.sleep(1000);
        Robot r = new Robot();
        int keyCode =  KeyEvent.VK_DOWN;
        r.keyPress(keyCode);
        r.keyRelease(keyCode);
        keyCode =  KeyEvent.VK_ENTER;
        r.keyPress(keyCode);
        r.keyRelease(keyCode);
        Thread.sleep(1000);
        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0,250)", "");
        driver.findElement(By.xpath("//input[@id='name']")).sendKeys("Test-1");
        Thread.sleep(1000);
        driver.findElement(By.id("date-range-field")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("date-range-field")).sendKeys("04 Jan, 2021 - 04 Jan, 2021");
        Thread.sleep(1000);
        driver.findElement(By.id("bid_currency")).sendKeys("0.4");
        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0,450)", "");
        driver.findElement(By.id("budget_daily_currency")).sendKeys("100");
        driver.findElement(By.id("budget_total_currency")).sendKeys("500");
        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0,1650)", "");
        Thread.sleep(1000);
        driver.findElement(By.xpath("//button[contains(text(),'Save')]")).click();


    }

    @Test(priority = 8)
    public static void New_campaign_group_8() throws InterruptedException, AWTException {
        driver.findElement(new By.ByLinkText("New campaign group")).click();
        Thread.sleep(3000);
        driver.findElement(By.id("select2-js-campaign-type-container")).click();
        Thread.sleep(1000);
        Robot r = new Robot();
        int keyCode =  KeyEvent.VK_DOWN;
        r.keyPress(keyCode);
        r.keyRelease(keyCode);
        keyCode =  KeyEvent.VK_ENTER;
        r.keyPress(keyCode);
        r.keyRelease(keyCode);
        Thread.sleep(1000);
        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0,250)", "");
        driver.findElement(By.xpath("//input[@id='name']")).sendKeys("Test-1");
        Thread.sleep(1000);
        driver.findElement(By.id("date-range-field")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("date-range-field")).sendKeys("04 Jan, 2021 - 04 Jan, 2021");
        Thread.sleep(1000);
        driver.findElement(By.id("bid_currency")).sendKeys("0.4");
        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0,450)", "");
        driver.findElement(By.id("budget_daily_currency")).sendKeys("100");
        driver.findElement(By.id("budget_total_currency")).sendKeys("500");
        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0,1650)", "");
        Thread.sleep(1000);
        driver.findElement(By.xpath("//button[contains(text(),'Save')]")).click();


    }

    @Test(priority = 9)
    public static void New_campaign_group_9() throws InterruptedException, AWTException {
        driver.findElement(new By.ByLinkText("New campaign group")).click();
        Thread.sleep(3000);
        driver.findElement(By.id("select2-js-campaign-type-container")).click();
        Thread.sleep(1000);
        Robot r = new Robot();
        int keyCode =  KeyEvent.VK_DOWN;
        r.keyPress(keyCode);
        r.keyRelease(keyCode);
        keyCode =  KeyEvent.VK_ENTER;
        r.keyPress(keyCode);
        r.keyRelease(keyCode);
        Thread.sleep(1000);
        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0,250)", "");
        driver.findElement(By.xpath("//input[@id='name']")).sendKeys("Test-1");
        Thread.sleep(1000);
        driver.findElement(By.id("date-range-field")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("date-range-field")).sendKeys("04 Jan, 2021 - 04 Jan, 2021");
        Thread.sleep(1000);
        driver.findElement(By.id("bid_currency")).sendKeys("0.4");
        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0,450)", "");
        driver.findElement(By.id("budget_daily_currency")).sendKeys("100");
        driver.findElement(By.id("budget_total_currency")).sendKeys("500");
        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0,1650)", "");
        Thread.sleep(1000);
        driver.findElement(By.xpath("//button[contains(text(),'Save')]")).click();


    }

    @Test(priority = 10)
    public static void New_campaign_group_10() throws InterruptedException, AWTException {
        driver.findElement(new By.ByLinkText("New campaign group")).click();
        Thread.sleep(3000);
        driver.findElement(By.id("select2-js-campaign-type-container")).click();
        Thread.sleep(1000);
        Robot r = new Robot();
        int keyCode =  KeyEvent.VK_DOWN;
        r.keyPress(keyCode);
        r.keyRelease(keyCode);
        keyCode =  KeyEvent.VK_ENTER;
        r.keyPress(keyCode);
        r.keyRelease(keyCode);
        Thread.sleep(1000);
        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0,250)", "");
        driver.findElement(By.xpath("//input[@id='name']")).sendKeys("Test-1");
        Thread.sleep(1000);
        driver.findElement(By.id("date-range-field")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("date-range-field")).sendKeys("04 Jan, 2021 - 04 Jan, 2021");
        Thread.sleep(1000);
        driver.findElement(By.id("bid_currency")).sendKeys("0.4");
        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0,450)", "");
        driver.findElement(By.id("budget_daily_currency")).sendKeys("100");
        driver.findElement(By.id("budget_total_currency")).sendKeys("500");
        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0,1650)", "");
        Thread.sleep(1000);
        driver.findElement(By.xpath("//button[contains(text(),'Save')]")).click();


    }

    @AfterTest
    public static void Test_Closure(){
        driver.quit();
    }


}
